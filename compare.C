void compare() {

  //---------------
  //Fullchain files
  TFile* full = new TFile("/global/cfs/cdirs/lz/sim/full/reconstructed/validation/odValidationSR1/BACCARAT-6.2.11_DER-9.1.0_LZAP-5.2.7_PROD-0/Alpha_ScintillatorCenter/202106/lz_202106301200_000050/rq/lz_000050_000000_lzap.root");
  TTree* Events = (TTree*)full->Get("Events");
  //Variables we care about
  int nP;
  vector<float> pArea;
  Events->SetMakeClass(1);
  Events->SetBranchAddress("pulsesODHG.nPulses", &nP);
  Events->SetBranchAddress("pulsesODHG.pulseArea_phd", &pArea);
  //Histograms
  TH1D* h_PA_all_full = new TH1D("h_PA_all_full", "Pulse area for all individual pulses; pulse area [phd]", 100, 0., 100.);
  for(int i = 0; i < Events->GetEntries(); i++) {
    if(i % 100 == 0) std::cout << "Full File: "<< i <<"/" << Events->GetEntries() << std::endl;
    Events->GetEntry(i);
    //    std::cout << "nPulses = " << nP << std::endl;
    for(int p = 0; p < pArea.size(); p++) {
      h_PA_all_full->Fill(pArea[p]);
    }
  }

  //---------------
  //Fast chain files  
  TFile* fast  = new TFile("/global/cfs/projectdirs/lz/users/amonte/lzlama/build/lz_Alpha_ScintillatorCenter_root_50590000.v0_LZLAMA_SR1_FlatTable.root");
  TTree* raw_od = (TTree*)fast->Get("raw_od");
  //Variables we care about
  vector<double>* signal = 0;
  raw_od->SetMakeClass(1);
  raw_od->SetBranchAddress("lzlama_raw.od.pulse.signal", &signal);
  //Histograms
  TH1D* h_PA_all_fast = new TH1D("h_PA_all_fast", "Pulse area for all individual pulses; pulse area [phd]", 100, 0., 100.);
  std::cout << (*signal).size();
  for(int i = 0; i < raw_od->GetEntries(); i++) {
    if(i % 1000 == 0) std::cout << "Fast File: " << i << "/" << raw_od->GetEntries() << std::endl;
    raw_od->GetEntry(i);
    for(int p = 0; p < (*signal).size(); p++) {
      h_PA_all_fast->Fill((*signal)[p]);
    }
  } 

  TCanvas* c1 = new TCanvas();
  c1->cd();
  h_PA_all_full->SetLineColor(kGreen+1);
  h_PA_all_full->Draw("HIST");
  h_PA_all_fast->Draw("HIST SAME");

  TFile* outfile = new TFile("valfile.root", "RECREATE");
  outfile->cd();
  h_PA_all_full->Write();
  h_PA_all_fast->Write();
  c1->Write("Comparison of all pulses");
  outfile->Write();
  outfile->Close();
}
